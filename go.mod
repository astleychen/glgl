module gitlab.com/gitlab-com/support/toolbox/glgl

go 1.12

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/xanzy/go-gitlab v0.20.1
)
