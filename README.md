# glgl

<strong>G</strong>it<strong>L</strong>ab **G**roup **L**eader is a tool for running pseudo-admin tasks for GitLab groups without admin access. It's mostly to do things that are too tedious to do manually via the UI or API.

## Usage

Help output:

```
GitLab Group Leader (glgl) is a tool for running pseudo-admin tasks for GitLab groups without admin access.
It's mostly to do things that are too tedious to do manually via the UI or by using the API.

Usage:
  glgl [command]

Available Commands:
  help        Help about any command
  rollcall    Recursively find all users in group.

Flags:
      --config-file string   config file (default is $HOME/.glgl.toml)
      --config-load string   load configuration set (default is 'default'
  -h, --help                 help for glgl

Use "glgl [command] --help" for more information about a command.
```

### Configuration

`glgl` requires a configuration file with the URL of the GitLab instance, authentication type, and the appropriate token or username/password to authenticate. `glgl` will use the settings in the default set by default, but you can add multiple configuration sets in the config file and specify which to use with `--config-load <set name>`. `glgl` expects the configuration file to be `~/.glgl.<filetype>`, where `<filetype>` is `toml`, `json`, or `yml`. An example configuration file can be found in the repo.

## Commands

At the moment, `glgl` only has one command. More will be added later on:

### Rollcall

**Note**: The rollcall script currently only includes direct members and does not count users within groups that have been invited as members. This script can be deprecated once https://gitlab.com/gitlab-org/gitlab/-/issues/35454 is implemented.

```
Recursively checks all subgroups and projects under a given group and finds all members.
Verbose flag will output more information about the members, but it takes notably longer.

Usage:

glgl rollcall <group_name_or_id>

Usage:
  glgl rollcall [flags]

Flags:
  -h, --help      help for rollcall
  -j, --json      output data in JSON. JSON outputs are more detailed.
  -v, --verbose   verbose output for both default and json.

Global Flags:
      --config-file string   config file (default is $HOME/.glgl.toml)
      --config-load string   load configuration set (default is 'default')
```

To run, simply do `glgl rollcall` then give it a group id or group full path. The `--verbose` flag grabs the user object instead of just the member object. If the report needs to be run on behalf of a customer or end-user you can redirect the output to a file to send. For example: 

`glgl rollcall <group_name_or_id> --verbose > ~/report.txt`
