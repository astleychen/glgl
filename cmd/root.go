package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string
var cfgLoad string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "glgl",
	Short: "Tool for group admin tasks on GitLab",
	Long: `GitLab Group Leader (glgl) is a tool for running pseudo-admin tasks for GitLab groups without admin access.
It's mostly to do things that are too tedious to do manually via the UI or by using the API.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config-file", "", "config file (default is $HOME/.glgl.toml)")
	rootCmd.PersistentFlags().StringVar(&cfgLoad, "config-load", "", "load configuration set (default is 'default')")
}

var configs map[string]string

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		viper.AddConfigPath(home)
		viper.SetConfigName(".glgl")
	}

	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if cfgLoad == "" {
		cfgLoad = "default"
	}
	configs = viper.GetStringMapString(cfgLoad)
	if len(configs) == 0 {
		fmt.Printf("error: no configuration set found for '%v'\n", cfgLoad)
		os.Exit(1)
	}
}
