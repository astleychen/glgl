package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
)

var rollcallCmd = &cobra.Command{
	Use:   "rollcall",
	Short: "Recursively find all users in group.",
	Long: `Recursively checks all subgroups and projects under a given group and finds all members.
Verbose flag will output more information about the members, but it takes notably longer.

Usage:

glgl rollcall <group_name_or_id>`,
	Run:  rollcall,
	Args: cobra.ExactArgs(1),
}

var verbose bool
var jsonOut bool

func init() {
	rootCmd.AddCommand(rollcallCmd)
	rollcallCmd.Flags().BoolVarP(&verbose, "verbose", "v", false, "verbose output for both default and json.")
	rollcallCmd.Flags().BoolVarP(&jsonOut, "json", "j", false, "output data in JSON. JSON outputs are more detailed.")
}

func rollcall(cmd *cobra.Command, args []string) {
	g, err := newClient()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	userMap := make(map[int]*User)
	gid := args[0]
	getMembersRecurse(g, gid, userMap)

	j, _ := cmd.Flags().GetBool("json")
	v, _ := cmd.Flags().GetBool("verbose")
	if j {
		if v {
			var users []*UserDetailed
			for _, user := range userMap {
				u, err := detailed(g, user)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				users = append(users, u)
			}
			jsonOutput(users)
		} else {
			var users []*User
			for _, user := range userMap {
				users = append(users, user)
			}
			jsonOutput(users)
		}
	} else {
		if v {
			for _, user := range userMap {
				ud, err := detailed(g, user)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				ud.defaultOutput()
			}
		} else {
			for _, user := range userMap {
				user.defaultOutput()
			}
		}
		fmt.Printf("Total Users: %v\n", len(userMap))
	}
}

func gnp(g []*Group, p []*Project) ([]string, []string) {
	var groups []string
	for _, group := range g {
		groups = append(groups, fmt.Sprintf("%v (%v)", group.FullPath, group.AccessLevel))
	}

	var projects []string
	for _, project := range p {
		projects = append(projects, fmt.Sprintf("%v (%v)", project.FullPath, project.AccessLevel))
	}

	return groups, projects
}

func (u *User) defaultOutput() {
	groups, projects := gnp(*u.Groups, *u.Projects)

	fmt.Printf("%v (@%v)\n", u.Name, u.Username)
	fmt.Printf("Groups:    [%v]\n", strings.Join(groups, ", "))
	fmt.Printf("Projects:  [%v]\n", strings.Join(projects, ", "))
	fmt.Println("")
}

func (u *UserDetailed) defaultOutput() {
	groups, projects := gnp(*u.Groups, *u.Projects)

	fmt.Printf("%v (@%v)\n", u.UserInfo.Name, u.UserInfo.Username)
	fmt.Printf("User ID:      \t%v\n", u.UserInfo.ID)
	fmt.Printf("Public Email: \t%v\n", u.UserInfo.PublicEmail)
	fmt.Printf("2FA Enabled:  \t%v\n", u.UserInfo.TwoFactorEnabled)
	fmt.Printf("State:        \t%v\n", u.UserInfo.State)
	fmt.Printf("Groups:       \t[%v]\n", strings.Join(groups, ", "))
	fmt.Printf("Projects:     \t[%v]\n", strings.Join(projects, ", "))
	fmt.Println("")
}

func jsonOutput(users interface{}) {
	bytes, err := json.MarshalIndent(users, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Printf("%s", bytes)
}

func detailed(g *gitlab.Client, user *User) (*UserDetailed, error) {
	userInfo, err := getUser(g, user.ID)
	if err != nil {
		return nil, err
	}
	userDetailed := &UserDetailed{
		UserInfo: userInfo,
		Projects: user.Projects,
		Groups:   user.Groups,
	}
	return userDetailed, nil
}
