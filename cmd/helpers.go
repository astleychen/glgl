package cmd

import (
	"fmt"

	gitlab "github.com/xanzy/go-gitlab"
)

func newClient() (*gitlab.Client, error) {
	authType, ok := configs["auth_type"]
	if !ok {
		return nil, fmt.Errorf("error: auth type needs to be set in config file\n")
	}

	switch authType {
	case "token":
		g := gitlab.NewClient(nil, configs["token"])
		g.SetBaseURL(configs["url"])
		return g, nil
	case "password":
		g, err := gitlab.NewBasicAuthClient(nil, configs["url"], configs["username"], configs["password"])
		if err != nil {
			return nil, err
		}
		return g, nil
	default:
		return nil, fmt.Errorf("error, %v is not a valid error type\n", authType)
	}
}

func nextPage(res *gitlab.Response, opt *gitlab.ListOptions) bool {
	if res.CurrentPage >= res.TotalPages {
		return false
	}

	opt.Page = res.NextPage
	return true
}

func getUser(g *gitlab.Client, uid int) (*gitlab.User, error) {
	user, _, err := g.Users.GetUser(uid)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func getProjects(g *gitlab.Client, gid interface{}) []*gitlab.Project {
	opt := &gitlab.ListGroupProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	var groupProjects []*gitlab.Project
	for {
		projects, res, err := g.Groups.ListGroupProjects(gid, opt)
		if err != nil {
			panic(err)
		}

		groupProjects = append(groupProjects, projects...)
		if !nextPage(res, &opt.ListOptions) {
			break
		}
	}
	return groupProjects
}

func getSubgroups(g *gitlab.Client, gid interface{}) []*gitlab.Group {
	opt := &gitlab.ListSubgroupsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	var subgroups []*gitlab.Group
	for {
		groups, res, err := g.Groups.ListSubgroups(gid, opt)
		if err != nil {
			panic(err)
		}

		subgroups = append(subgroups, groups...)
		if !nextPage(res, &opt.ListOptions) {
			break
		}
	}
	return subgroups
}

type Member struct {
	ID          int                     `json:"id"`
	Username    string                  `json:"username"`
	Name        string                  `json:"name"`
	State       string                  `json:"state"`
	AccessLevel gitlab.AccessLevelValue `json:"access_level"`
}

func getGroupMembers(g *gitlab.Client, gid interface{}) []*Member {
	opt := &gitlab.ListGroupMembersOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	var groupMembers []*gitlab.GroupMember
	for {
		gms, res, err := g.Groups.ListGroupMembers(gid, opt)
		if err != nil {
			panic(err)
		}

		groupMembers = append(groupMembers, gms...)
		if !nextPage(res, &opt.ListOptions) {
			break
		}
	}

	var members []*Member
	for _, member := range groupMembers {
		m := &Member{
			ID:          member.ID,
			Username:    member.Username,
			Name:        member.Name,
			State:       member.State,
			AccessLevel: member.AccessLevel,
		}
		members = append(members, m)
	}
	return members
}

func getProjectMembers(g *gitlab.Client, pid interface{}) []*Member {
	opt := &gitlab.ListProjectMembersOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	var projectMembers []*gitlab.ProjectMember
	for {
		pms, res, err := g.ProjectMembers.ListProjectMembers(pid, opt)
		if err != nil {
			panic(err)
		}

		projectMembers = append(projectMembers, pms...)
		if !nextPage(res, &opt.ListOptions) {
			break
		}
	}

	var members []*Member
	for _, member := range projectMembers {
		m := &Member{
			ID:          member.ID,
			Username:    member.Username,
			Name:        member.Name,
			State:       member.State,
			AccessLevel: member.AccessLevel,
		}
		members = append(members, m)
	}
	return members
}

type Project struct {
	FullPath    string
	AccessLevel string
}

type Group struct {
	FullPath    string
	AccessLevel string
}

type User struct {
	ID       int         `json:"id"`
	Username string      `json:"username"`
	Name     string      `json:"name"`
	State    string      `json:"state"`
	Projects *[]*Project `json:"projects"`
	Groups   *[]*Group   `json:"groups"`
}

type UserDetailed struct {
	UserInfo *gitlab.User `json:"user_info"`
	Projects *[]*Project  `json:"projects"`
	Groups   *[]*Group    `json:"groups"`
}

var accessLevels = map[int]string{
	10: "Guest",
	20: "Reporter",
	30: "Developer",
	40: "Maintainer",
	50: "Owner",
	0:  "None",
}

func getMembersRecurse(g *gitlab.Client, gid interface{}, um map[int]*User) {
	groupMembers := getGroupMembers(g, gid)
	group, _, err := g.Groups.GetGroup(gid)
	if err != nil {
		panic(err)
	}
	groupPath := group.FullPath
	for _, gm := range groupMembers {
		groupStruct := &Group{groupPath, accessLevels[int(gm.AccessLevel)]}
		if member, exists := um[gm.ID]; exists {
			*member.Groups = append(*member.Groups, groupStruct)
		} else {
			emptyProjectSlice := make([]*Project, 0)
			um[gm.ID] = &User{
				ID:       gm.ID,
				Username: gm.Username,
				Name:     gm.Name,
				State:    gm.State,
				Groups:   &[]*Group{groupStruct},
				Projects: &emptyProjectSlice,
			}
		}
	}

	projects := getProjects(g, gid)
	for _, project := range projects {
		projectPath := project.PathWithNamespace
		projectMembers := getProjectMembers(g, project.ID)
		for _, pm := range projectMembers {
			projectStruct := &Project{projectPath, accessLevels[int(pm.AccessLevel)]}

			if member, exists := um[pm.ID]; exists {
				*member.Projects = append(*member.Projects, projectStruct)
			} else {
				emptyGroupSlice := make([]*Group, 0)
				um[pm.ID] = &User{
					ID:       pm.ID,
					Username: pm.Username,
					Name:     pm.Name,
					State:    pm.State,
					Projects: &[]*Project{projectStruct},
					Groups:   &emptyGroupSlice,
				}
			}
		}
	}

	subgroups := getSubgroups(g, gid)
	if len(subgroups) > 0 {
		for _, sg := range subgroups {
			getMembersRecurse(g, sg.ID, um)
		}
	}
}
